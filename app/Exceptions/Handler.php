<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Client\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->renderable(function (ValidationException $exception) {
            return response()->json([
                'error' => [
                    'message' => $exception->validator->getMessageBag()->getMessages(),
                    'status'  => $exception->status
                ]
            ],$exception->status);
        });
        $this->renderable(function (Throwable $exception) {
            return response()->json([
                'error' => [
                    'message' => $exception->getMessage(),
                    'status'  => 500
                ]
            ],500);

        });

    }



}
