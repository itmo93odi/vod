<?php

namespace App\Http\Controllers;


use App\Models\User;
use App\Rules\CheckPassword;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class AuthController extends Controller
{
    /**
     * create register user a newly.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function register(Request $request)
    {
        $this->validate($request, [
            'name'    => 'required|string',
            'email'    => 'required|unique:users,email,NULL',
            'password' => 'required',
            'mobile'   => 'regex:/(09)[0-9]{9}/|digits:11|numeric',
            'address'  => 'string',
        ]);

        $email=request('email');
        $password=request('password');
        $mobile=request('mobile');
        $address=request('address');
        $name=request('address');


        //create User
        $user = User::create([
            'email' => $email,
            'password' => $password,
            'mobile' => $mobile,
            'address' => $address,
            'name' => $name,
        ]);


        $result = $user->createToken($request->email, ['user']);

        return response()->json([
            'data' => [
                'token_type' => 'Bearer ',
                'token' => $result->plainTextToken,
                'scope' => $result->accessToken->abilities,
                'user_id' => $user->id
            ]
        ], ResponseAlias::HTTP_CREATED);
    }


    /**
     * create login token created.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'email'    => 'required|exists:users,email',
            'password' => ['required',new CheckPassword()],
        ]);

        $email=request('email');

        $user=User::whereEmail($email)->first();

        $result = $user->createToken($request->email, ['user']);

        return response()->json([
            'data' => [
                'token_type' => 'Bearer ',
                'token' => $result->plainTextToken,
                'scope' => $result->accessToken->abilities,
                'user_id' => $user->id
            ]
        ], ResponseAlias::HTTP_CREATED);
    }


}
