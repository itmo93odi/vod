<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeleteCommentRequest;
use App\Http\Requests\StoreCommentRequest;
use App\Http\Requests\UpdateCommentRequest;
use App\Http\Resources\CommentResource;
use App\Repositories\CommentRepository;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{

    public function __construct(private CommentRepository $commentRepository)
    {
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $limit = request('limit') ?: 20;
        $page = request('page') ?: 1;
        $user=Auth::user()->id;

        $comments=$this->commentRepository->getByUser($user,$limit,$page);

        return CommentResource::collection($comments);
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCommentRequest $request)
    {

        $user_id=Auth::user()->id;
        $request->merge([
            'user_id' => $user_id
        ]);

        $comments=$this->commentRepository->createComment($request->all());

        return new CommentResource($comments);

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {

        $comment=$this->commentRepository->getOneById($id);

        return new CommentResource($comment);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCommentRequest $request, string $comment_id)
    {
        $description=$request->get('description');

        $this->commentRepository->updateComment($comment_id,$description);

        return response()->json([
            'data' => [
                'message' => __('message.update',['name'=>'کامنت']),
                'status' => 200
            ]
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(DeleteCommentRequest $request ,string $id)
    {

        $this->commentRepository->deleteByUser($id);

        return response()->json([
            'data' => [
                'message' => __('message.delete',['name'=>'کامنت']),
                'status' => 200
            ]
        ]);
    }
}
