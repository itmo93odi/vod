<?php

namespace App\Http\Controllers;


use App\Http\Resources\GenreResource;
use App\Repositories\GenreRepository;

class GenreController extends Controller
{
    public function __construct(
        private GenreRepository $genreRepository
    )
    {
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $genres=$this->genreRepository->getAll();

        return GenreResource::collection($genres);
    }

}
