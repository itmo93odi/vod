<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMarkRequest;
use App\Http\Resources\MovieResource;
use App\Models\Movie;
use App\Repositories\MovieRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;

class MarkController extends Controller
{

    public function __construct(
        private UserRepository $userRepository ,
        private MovieRepository $movieRepository
    )
    {
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $limit = request('limit');
        $page = request('page');
        $user_id=Auth::user()->id;

        $movies=$this->movieRepository->getMovieMarked($user_id,$limit,$page);

        return MovieResource::collection($movies);
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreMarkRequest $request)
    {
        $movie_id = request('movie_id');
        $user_id=Auth::user()->id;

        $this->userRepository->storeMarkMovieByUser($user_id,$movie_id);

        return response()->json([
            'data' => [
                'message' => __('message.mark_store'),
                'status' => 200
            ]
        ]);
    }



}
