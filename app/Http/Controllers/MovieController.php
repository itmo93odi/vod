<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMovieRequest;
use App\Http\Resources\MovieResource;
use App\Repositories\FileRepository;
use App\Repositories\MovieRepository;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    public function __construct(
        private MovieRepository $movieRepository,
        private FileRepository $fileRepository
    )
    {
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $limit = request('limit');
        $page = request('page');

        $movies=$this->movieRepository->getWithFilter($request,$limit,$page);

        return MovieResource::collection($movies);
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreMovieRequest $request)
    {

        $files = request('image');

        $movie=$this->movieRepository->createMovie($request->all());

        $this->fileRepository->uploadFile($movie,$files);

        return new MovieResource($movie);
    }

}
