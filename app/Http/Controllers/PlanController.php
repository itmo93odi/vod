<?php

namespace App\Http\Controllers;

use App\Http\Resources\PlanResource;
use App\Repositories\PlanRepository;

class PlanController extends Controller
{

    public function __construct(
        private PlanRepository $planRepository
    )
    {
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $plans = $this->planRepository->getByPaginate();

        return PlanResource::collection($plans);
    }




}
