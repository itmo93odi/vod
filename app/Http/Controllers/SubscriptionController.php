<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSubscriptionRequest;
use App\Http\Resources\SubscriptionResource;
use App\Repositories\PlanRepository;
use App\Repositories\SubscriptionRepository;
use Illuminate\Support\Facades\Auth;

class SubscriptionController extends Controller
{

    public function __construct(
        private SubscriptionRepository $subscriptionRepository,
        private PlanRepository $planRepository
    )
    {
    }


    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $limit = request('limit') ?: 20;
        $page = request('page') ?: 1;
        $user_id = Auth::user()->id;

        $sub = $this->subscriptionRepository->getByUser($user_id, $limit, $page);

        return SubscriptionResource::collection($sub);
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreSubscriptionRequest $request)
    {

        $plan_id = $request->get('plan_id');
        $user_id = Auth::user()->id;

        $plan = $this->planRepository->getOne($plan_id);
        $sub = $this->subscriptionRepository->getOne($user_id);
        $calculate = $this->subscriptionRepository->calculateSubscriptionByUser($plan, $sub->ends_at ?? null);
        $this->subscriptionRepository->deleteByUser($user_id);

        $this->subscriptionRepository->createByUser($user_id, $plan->id, $calculate['starts_at'], $calculate['ends_at']);


        return response()->json([
            'data' => [
                'message' => __('message.sub_store', ['start' => $calculate['starts_at'], 'end' => $calculate['ends_at']]),
                'status' => 200
            ]
        ]);

    }


}
