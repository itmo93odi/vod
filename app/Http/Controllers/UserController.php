<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Repositories\PlanRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function __construct(
        private UserRepository $userRepository
    )
    {
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $limit = request('limit') ?: 20;
        $page = request('page') ?: 1;

        $users=$this->userRepository->getByFilter($request,$limit,$page);

        return UserResource::collection($users);
    }
}
