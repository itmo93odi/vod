<?php

namespace App\Http\Controllers;


use App\Http\Requests\StoreViewRequest;
use App\Http\Resources\MovieResource;
use App\Repositories\MovieViewRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ViewController extends Controller
{
    public function __construct(
        private MovieViewRepository $viewRepository,
        private UserRepository $userRepository,
    )
    {
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $limit = request('limit');
        $page = request('page');
        $user_id =Auth::user()->id;
        $movies=$this->viewRepository->getByUser($user_id,$limit,$page);
        return MovieResource::collection($movies);
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreViewRequest $request)
    {

        $movie_id = request('movie_id');
        $user_id = Auth::user()->id;

        $this->userRepository->storeViewMovieByUser($user_id,$movie_id);

        return response()->json([
            'data' => [
                'message' => __('message.view_store'),
                'status' => 200
            ]
        ]);

    }


}
