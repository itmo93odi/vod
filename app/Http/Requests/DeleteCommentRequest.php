<?php

namespace App\Http\Requests;

use App\Repositories\CommentRepository;
use Illuminate\Foundation\Http\FormRequest;

class DeleteCommentRequest extends FormRequest
{
    public function __construct(private CommentRepository $commentRepository)
    {
    }
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        $comment_id=$this->route('comment');
        $comment = $this->commentRepository->getOneById($comment_id);
        return $comment && $comment->user_id == $this->user()->id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            //
        ];
    }
}
