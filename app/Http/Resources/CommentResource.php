<?php


namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'description' => $this->description,
            'movie' => new MovieResource($this->movie),
            'user' => new UserResource($this->user),
        ];
    }


}
