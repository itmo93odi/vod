<?php


namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class FileResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'file' => env('FILE_SERVER_HOST').optional($this)->path,
        ];
    }


}
