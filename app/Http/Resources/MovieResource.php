<?php


namespace App\Http\Resources;


use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class MovieResource extends JsonResource
{

    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'release_date' => $this->release_date,
            'revenue' => $this->revenue,
            'budget' => $this->budget,
            'images' => FileResource::collection($this->file),
            'genre' => GenreResource::collection($this->genre),
            'created_at' => $this->created_at
        ];
    }


}
