<?php


namespace App\Http\Resources;


use App\Models\Plan;
use Illuminate\Http\Resources\Json\JsonResource;

class SubscriptionResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => new UserResource($this->user),
            'plan' => new PlanResource($this->plan),
            'starts_at' => $this->starts_at,
            'ends_at' => $this->ends_at,
        ];
    }


}
