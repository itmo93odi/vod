<?php

namespace App\Models;

use App\Traits\FilterTrait;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Movie extends Model
{
    use HasFactory,FilterTrait,HasUuids;

    protected $fillable = [
        'movie_id',
        'name',
        'description',
        'release_date',
        'revenue',
        'budget'
    ];

    /**
     * Get the post's image.
     */
    public function file()
    {
        return $this->morphMany(File::class, 'parentable');
    }
    /**
     * Get the movie's view.
     */
    public function view(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(User::class,'movie_views');
    }

    /**
     * Get the movie's genre.
     */
    public function genre(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Genre::class,'movie_genres');
    }

    /**
     * Get the mark's movie.
     */
    public function mark(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(User::class,'marks');
    }


}
