<?php

namespace App\Repositories;

use App\Models\Comment;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class CommentRepository extends Repository
{


    /** @return LengthAwarePaginator */
    public function getByUser($id,$limit=20,$page=1): LengthAwarePaginator
    {
        return Comment::query()
            ->whereHas('user',function($query) use ($id){
                $query->where('users.id','=',$id);
            })
            ->orderBy('id', 'DESC')
            ->paginate($limit, '*', 'page', $page);
    }

    public function createComment($params)
    {
        return Comment::create($params);
    }

    public function updateComment($comment_id,$description)
    {
        return Comment::whereId($comment_id)
            ->update([
            'description' => $description,
        ]);
    }

    public function deleteByUser($comment_id)
    {
        $comment=Comment::whereId($comment_id)->first();
        return $comment->delete();

    }

}
