<?php

namespace App\Repositories;


use App\Models\Movie;
use Illuminate\Support\Facades\Storage;

class FileRepository extends Repository
{

    public function uploadFile(Movie $movie, $file)
    {
        $file_name = time() . '-' . $movie->movie_id;
        $extension = $file->getClientOriginalExtension();
        $size = $file->getSize();
        $path = 'public/images/movie/'. $file_name . '.' . $extension;
        $file = Storage::put($path, file_get_contents($file));
        if ($file)
            $movie->file()->updateOrCreate([
                'parentable_type' => Movie::class,
                'parentable_id' => $movie->movie_id,
                'path' => $path,
                'size' => $size,
                'type' => 'image',
                'extension' => $extension
            ]);

    }
}
