<?php

namespace App\Repositories;

use App\Models\Movie;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

class MovieRepository extends Repository
{


    /** @return LengthAwarePaginator */
    public function getWithFilter(Request $request,$limit=20,$page=1): LengthAwarePaginator
    {
        $genre=$request->get('genre');
        return  Movie::query()
            ->with('genre')
            ->filter($request)
            ->when($genre,function ($query) use ($genre) {
                $query->whereHas('genre',function($query) use ($genre){
                    $query->where('genres.name',$genre);
                });
            })
            ->orderBy('id', 'DESC')
            ->paginate($limit, '*', 'page', $page);
    }

    /** @return LengthAwarePaginator */
    public function getMovieMarked($id,$limit=20,$page=1): LengthAwarePaginator
    {
        return Movie::query()
            ->whereHas('mark',function($query) use ($id){
                $query->where('users.id','=',$id);
            })
            ->orderBy('movies.id', 'DESC')
            ->paginate($limit, '*', 'page', $page);
    }


    public function createMovie(array $params)
    {
        return Movie::create($params);

    }

}
