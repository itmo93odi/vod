<?php

namespace App\Repositories;




use App\Models\Movie;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class MovieViewRepository extends Repository
{


    /** @return LengthAwarePaginator */
    public function getByUser($id, $limit = 20, $page = 1): LengthAwarePaginator
    {
        return Movie::query()
            ->whereHas('view', function ($query) use ($id) {
                $query->where('users.id', '=', $id);
            })
            ->orderBy('movies.id', 'DESC')
            ->paginate($limit, '*', 'page', $page);
    }


    public function getByPaginate($limit = 20, $page = 1)
    {

    }





}
