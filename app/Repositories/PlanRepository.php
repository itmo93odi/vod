<?php

namespace App\Repositories;

use App\Models\Plan;

class PlanRepository extends Repository
{


    public function getOne($id)
    {
        return Plan::query()
            ->whereId($id)
            ->first();
    }


    public function getByPaginate($limit = 20, $page = 1)
    {
        return Plan::query()
            ->orderBy('id', 'DESC')
            ->paginate($limit, '*', 'page', $page);
    }





}
