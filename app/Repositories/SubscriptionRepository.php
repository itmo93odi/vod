<?php

namespace App\Repositories;

use App\Models\Plan;
use App\Models\Subscription;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class SubscriptionRepository extends Repository
{


    public function getOne($id)
    {
        return Subscription::query()
            ->whereUserId($id)
            ->first();
    }


    /** @return LengthAwarePaginator */
    public function getByUser($id, $limit = 20, $page = 1): LengthAwarePaginator
    {
        return Subscription::query()
            ->whereHas('user', function ($query) use ($id) {
                $query->where('users.id', '=', $id);
            })
            ->orderBy('subscriptions.id', 'DESC')
            ->paginate($limit, '*', 'page', $page);
    }


    /**
     * Calculate Subscriptions
     * @param $id
     * @param Plan $plan
     * @param $ends_at
     * @return array
     */
    public function calculateSubscriptionByUser(Plan $plan, $ends_at = null): array
    {
        $start_at = Carbon::now();
        $time = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now());
        $period = $ends_at ? $time->diff($ends_at)->days + $plan->period : $plan->period;
        $ends_at = $time->addDays($period);
        return [
            'starts_at' => $start_at,
            'ends_at' => $ends_at
        ];
    }


    public function createByUser(string $id, int $plan_id, Carbon $start_at, Carbon $ends_at)
    {
        return Subscription::create([
            'user_id' => $id,
            'plan_id' => $plan_id,
            'starts_at' => $start_at,
            'ends_at' => $ends_at
        ]);

    }


    public function deleteByUser(string $id)
    {

        return Subscription::query()
            ->whereUserId($id)
            ->delete();

    }
}
