<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Http\Request;

class UserRepository extends Repository
{

    public function storeViewMovieByUser(string $id,$movie_id)
    {
        $users = User::whereId($id)->first();
        return $users->view()->attach([$movie_id]);
    }
    public function storeMarkMovieByUser($id,$movie_id){
        $users = User::whereId($id)->first();
        return $users->mark()->syncWithoutDetaching([$movie_id]);
    }

    public function getByFilter(Request $request,$limit=20 , $page=1){
        return User::query()
            ->filter($request)
            ->orderBy('id', 'DESC')
            ->paginate($limit, '*', 'page', $page);
    }
}
