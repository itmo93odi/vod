<?php


namespace App\Traits;


use Illuminate\Http\Request;
use Illuminate\Support\Str;

/**
 * Trait FilterTrait
 * @package App\Traits
 */
trait FilterTrait
{
    /**
     * @var
     */
    protected $query;
    /**
     * @var
     */
    protected $filter;
    /**
     * @var
     */
    protected $key;

    /**
     * @param $query
     * @param Request $request
     * @return mixed
     */
    public function scopeFilter($query,Request $request)
    {
        $this->query = $query;
        $this->filter = $request->all();


        foreach ($this->filter as $key => $item) {

            if($this->isJSON($item)) {
                $condition = json_decode($item);
                $this->key = $key;
                if (method_exists(FilterTrait::class, $condition->type)) {
                    $this->{$condition->type}();
                }
            }
        }

        return $this->query;
    }


    /**
     *
     */
    public function between()
    {
        $price = json_decode($this->filter[$this->key]);
        $this->query->whereBetween($this->key, [$price->from,$price->to]);
    }

    /**
     *
     */
    public function exec()
    {
        $value=json_decode($this->filter[$this->key]);
        $condition = isset($value->condition) ? $value->condition : '=';
        $this->query->where($this->key,$condition,$value->value);
    }

    /**
     *
     */
    public function execIn()
    {
        $value=json_decode($this->filter[$this->key]);
        $value=explode(',',$value->value);
        $this->query->whereIn($this->key,$value);
    }



    /**
     *
     */
    public function contains()
    {
        $search = json_decode($this->filter['search']);
        $text = $search->text;
        $this->query->where(function ($query) use ($search,$text) {
            foreach ($search->columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $text . '%');
            }
        });
    }


    public function sort(){
        $value=json_decode($this->filter[$this->key]);
        $this->query->orderBy($this->key,$value->value);
    }




    /**
     * @param $string
     * @return bool
     */
    function isJSON($string){
        return is_string($string) && is_array(json_decode($string, true)) ? true : false;
    }

}
