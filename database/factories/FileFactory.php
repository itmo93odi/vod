<?php

namespace Database\Factories;

use App\Models\Movie;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\UploadedFile;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\File>
 */
class FileFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {

        $nameImage=time().'_movie';
        $pathImage='public/images';

        $path=UploadedFile::fake()->create('image.jpg')
            ->storeAs($pathImage, $nameImage.'.jpg');

        return [
            'parentable_type' => Movie::class ,
            'parentable_id' => fake()->randomElement(Movie::all()->pluck('id')),
            'path' => $path,
            'extension' => 'jpg',
            'type' => 'image',
            'size' => 200 ,
        ];
    }
}
