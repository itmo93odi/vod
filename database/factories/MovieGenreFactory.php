<?php

namespace Database\Factories;

use App\Models\Genre;
use App\Models\Movie;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class MovieGenreFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'movie_id' => fake()->randomElement(Movie::all()->pluck('id')),
            'genre_id' => fake()->randomElement(Genre::all()->pluck('id'))
        ];
    }
}
