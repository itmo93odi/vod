<?php

namespace Database\Factories;

use App\Models\Plan;
use App\Models\User;
use DateInterval;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class SubscriptionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {

        return [
            'user_id' => fake()->randomElement(User::all()->pluck('id')) ,
            'plan_id' => fake()->randomElement(Plan::all()->pluck('id')) ,
            'starts_at' => fake()->dateTimeBetween('-7 Days')->format('Y-m-d H:i:s') ,
            'ends_at' => fake()->dateTimeBetween('+7 Days','+1 Month')->format('Y-m-d H:i:s')
        ];
    }
}
