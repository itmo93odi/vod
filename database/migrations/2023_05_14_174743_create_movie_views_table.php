<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('movie_views', function (Blueprint $table) {
            $table->id();

            $table->char('user_id')->index();
            $table->foreign('user_id')
                ->on('users')
                ->references('id')->onDelete('cascade');

            $table->char('movie_id')->index();
            $table->foreign('movie_id')
                ->on('movies')
                ->references('id')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('movie_views');
    }
};
