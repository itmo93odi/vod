<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
         \App\Models\User::factory(10)->create();
         \App\Models\Movie::factory(20)->create();
         \App\Models\Plan::factory(2)->create();
         \App\Models\Genre::factory(6)->create();
         \App\Models\Comment::factory(6)->create();
         \App\Models\Subscription::factory(6)->create();
         \App\Models\Mark::factory(20)->create();
         \App\Models\MovieGenre::factory(20)->create();
         \App\Models\MovieView::factory(20)->create();
         \App\Models\File::factory(10)->create();

    }
}
