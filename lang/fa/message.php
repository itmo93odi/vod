<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Custom Text Message
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'view_store' => 'فیلم مورد نظر به لیست ویدیوهای دیده شده کاربر اضافه شد',
    'mark_store' => 'فیلم مورد نظر به لیست علاقمندی های کاربر اضافه شد',
    'sub_store' => 'اشتراک کاربر از تاریخ :start تا :end فعال گردید',
    'update' => 'اطلاعات :name با موفقیت ویرایش شد',
    'delete' => 'اطلاعات :name با موفقیت حذف گردید',
    'store' => 'اطلاعات :name با موفقیت ثبت گردید',
];
