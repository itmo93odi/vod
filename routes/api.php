<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\MarkController;
use App\Http\Controllers\MovieController;
use App\Http\Controllers\PlanController;
use App\Http\Controllers\SubscriptionController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ViewController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->group(function () {

});


Route::resource('user', UserController::class)->only(['index','store']);
Route::resource('comment', CommentController::class)->only(['index','show','store','destroy','update']);
Route::resource('subscription', SubscriptionController::class)->only(['index','store']);
Route::resource('plan', PlanController::class)->only(['index']);
Route::resource('genre', GenreController::class)->only('index');
Route::resource('view', ViewController::class)->only(['index','store']);
Route::resource('mark', MarkController::class)->only(['index','store']);
Route::resource('movie', MovieController::class)->only(['index','store']);




Route::post('auth/register', [AuthController::class, 'register']);
Route::post('auth/login', [AuthController::class, 'login'])->name('login');
