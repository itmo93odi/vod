<?php

namespace Tests\Feature;

use App\Models\Comment;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Tests\TestCase;

class CommentTest extends TestCase
{
    /**
     * A basic feature test example.
     * @test
     */
    public function test_get_comment_all (): void
    {
        Comment::factory(3)->create();

        $response = $this->get('/api/comment');


        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id', 'description', 'movie', 'user'
                    ]
                ]
            ]);

        $response->assertStatus(200);
    }

    /**
     * check if a comment not valid.
     * @test
     * @return void
     */
    public function check_if_a_comment_not_valid()
    {

        $response = $this->get('/api/comment/-1');

        $response->assertStatus(404);

    }

    /**
     * check if not  found delete comment.
     * @test
     * @return void
     */
    public function check_if_not_found_update_comment()
    {

        $response = $this->put('api/comment/-1');

        $response->assertStatus(ResponseAlias::HTTP_NOT_FOUND);
    }

    /**
     * delete comment.
     * @test
     * @return void
     */
    public function can_delete_a_comment()
    {
        $comment = Comment::factory(1)->create();

        $response = $this->delete('api/comment/' . $comment->id);

        $response->assertStatus(ResponseAlias::HTTP_OK);

        $this->assertSoftDeleted('comments', ['id' => $comment->id]);
    }

    /**
     * check if not  found delete comment.
     * @test
     * @return void
     */
    public function check_if_not_found_delete_comment()
    {

        $response = $this->delete('api/comment/-1');
        $response->assertStatus(ResponseAlias::HTTP_NOT_FOUND);
    }
}
