<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     * @test
     */
    public function can_get_all_users(): void
    {
        User::factory(3)->create();

        $response = $this->get('/api/user');


        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id', 'name', 'email', 'mobile','address',
                        ]
                ]
            ]);
    }
}
